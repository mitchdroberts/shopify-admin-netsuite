/**
 * client.js
 * @NApiVersion 2.1
 * @NModuleScope public
 *
 * Module is intended to act like an SDK for the Shopify Admin API
 * PATH: SuiteScripts/shopifySDK/client
 *
 * @author Mitch Roberts
 * @version 2019.0.1, 25 Nov 2019
 */
// Copy .config into config to set Shopify configuration data
define([
    './src/config',
    './src/resources/api-resource',
    './src/resources/stringify-resource',
    './src/root/variant-root',
    './src/root/product-root'
], function (_config, _api, _stringify, _variant, _product) {
    let {url, auth} = _config;

    /**
     * Sets Global name space for SDK
     *
     * @namespace
     * @typedef {Class} ClassRef
     */
    const Client = {};

    /**
     * Class definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.Collection = null;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.Customer = null;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.DraftOrder = null;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.Fulfillment = null;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.InventoryItem = null;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.Location = null;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.Order = null;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.ProductVariant = _variant;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.Product = _product;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.Refund = null;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.Shop = null;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.Query = _api.build({url: url, auth: auth});


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.MutationQuery = _api.build({url: url, auth: auth});


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.StringifyQuery = _stringify.StringifyQuery;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client.StringifyMutation = _stringify.StringifyMutation;


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2019.2
     */
    Client._setConfig = ({_url, _auth}) => { url = _url; auth = _auth; };

    return Client;
});


/*
require(['client'], function(Client) {
    const Variant = new Client.Variant();
    let variantObject = Variant.build({
        init: function() {
            this.appendParameters({
                first: 1
            });
            this.appendField(Variant.fields.title());
            this.appendField(Variant.fields.sku());
            this.appendConnection(Variant.ConnectedFields.PRODUCT(
                new Client.Variant().build({
                    init: function() {
                        this.appendField('id');
                    }
                })
            ));
            this.appendRecord(Variant.ConnectedRecords.METAFIELDS(
                new Client.Variant().build({
                    init: function() {
                        this.appendParameters({
                            first: 2
                        });
                        this.appendField(Variant.fields.sku());
                        this.appendRecord(Variant.ConnectedRecords.METAFIELDS(
                            new Client.Variant().build({
                                init: function() {
                                    this.appendParameters({
                                        first: 9
                                    });
                                    this.appendField(Variant.fields.id());
                                }
                            })
                        ));
                    }
                })
            ));
        }
    });
    //console.log(variantObject);
    console.log(Variant.generateQuery.ALL(variantObject));
});
*/