/**
 * client.js
 * @NApiVersion 2.0
 * @NModuleScope public
 *
 * Module is intended to act like an SDK for the Shopify Admin API
 * PATH: SuiteScripts/ryosuitescript/custmods/shopifySDK/client
 *
 * @author Mitch Roberts
 * @version 2019.0.1, 25 Nov 2019
 */

"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }
function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

define(['N/record', 'N/https', './api-resource', './variant-resource'], function (_record, _https, APIClass, Variant) {
    var Client =
        /*#__PURE__*/
        function () {
            _createClass(Client, null, [{
                key: "buildClient",
                value: function buildClient(config) {
                    return new Client(config);
                }
            }]);

            function Client() {
                var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
                    url: "https://ryonet.myshopify.com/admin/api/graphql.json",
                    auth: "9868ba545dcc661e4df9debf3f046c1d"
                };

                _classCallCheck(this, Client);

                this.url = config.url || url;
                this.auth = config.auth || auth;
                this.variant = new Variant(config.url, config.auth);
                this.query = new APIClass({
                    url: config.url,
                    auth: config.auth
                });
            }

            return Client;
        }();

    return Client;
});
