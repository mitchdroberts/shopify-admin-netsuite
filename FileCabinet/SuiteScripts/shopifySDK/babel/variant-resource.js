/**
 * client.js
 * @NApiVersion 2.0
 * @NModuleScope public
 *
 * Module is intended to act like an SDK for the Shopify Admin API
 * PATH: SuiteScripts/shopifySDK/babel/client
 *
 * @author Mitch Roberts
 * @version 2019.0.1, 25 Nov 2019
 */

"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }
function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

define([], function () {
    var _temp;

    var Variant = (_temp = function Variant() {
        _classCallCheck(this, Variant);

        _defineProperty(this, "fetchAll", function () {
            var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
                _ref$first = _ref.first,
                first = _ref$first === void 0 ? 1 : _ref$first,
                _ref$fields = _ref.fields,
                fields = _ref$fields === void 0 ? 'id' : _ref$fields;

            var body = "{ productVariants(first: ".concat(first, "){ edges { node { ").concat(fields, " } } } }");
            return body;
        });

        _defineProperty(this, "fetchById", function () {});

        _defineProperty(this, "fetchBySku", function (_ref2) {
            var productSku = _ref2.productSku,
                _ref2$first = _ref2.first,
                first = _ref2$first === void 0 ? 1 : _ref2$first,
                _ref2$fields = _ref2.fields,
                fields = _ref2$fields === void 0 ? 'id' : _ref2$fields;
            var body = "{ productVariants(first: ".concat(first, ", query: \"sku:'").concat(productSku, "'\"){ edges { node { ").concat(fields, " } } } }");
            return body;
        });
    }, _temp);
    return Variant;
});
