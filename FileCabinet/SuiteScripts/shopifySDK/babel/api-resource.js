/**
 * client.js
 * @NApiVersion 2.0
 * @NModuleScope public
 *
 * Module is intended to act like an SDK for the Shopify Admin API
 * PATH: SuiteScripts/ryosuitescript/custmods/shopifySDK/client
 *
 * @author Mitch Roberts
 * @version 2019.0.1, 25 Nov 2019
 */

"use strict";
function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }
function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

define(['N/https'], function (_https) {
    var _temp;

    var APIClass = (_temp = function APIClass(_ref) {
        var _this = this;

        var url = _ref.url,
            auth = _ref.auth;

        _classCallCheck(this, APIClass);

        _defineProperty(this, "send", function (data) {
            var response = _https.post({
                url: _this.url,
                body: data,
                headers: {
                    "Content-Type": "application/graphql",
                    "X-Shopify-Access-Token": _this.auth
                }
            });

            if (response.code === 200) return JSON.parse(response.body).data.productVariants.edges;else throw new Error(response.toString());
        });

        this.url = url;
        this.auth = auth;
    }, _temp);
    return APIClass;
});
