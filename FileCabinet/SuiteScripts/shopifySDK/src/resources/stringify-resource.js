/**
 * api-resource.js
 * @NApiVersion 2.1
 * @NModuleScope public
 *
 * Module is intended to act like an SDK for the Shopify Admin API
 * PATH: SuiteScripts/shopifySDK/api-resource
 *
 * @author Mitch Roberts
 * @version 2019.0.1, 25 Nov 2019
 */
define([], function(){
    return {
        StringifyQuery: ({variantQuery}) => `{${stringBuilder(variantQuery)}}`,
        StringifyMutation: ({variantMutation}) => `mutation{${stringBuilder(variantMutation)}}`
    };
});

const getMutationInput = (args) => {
    let data = Object.keys(args.input).reduce((str, key) => str + `${key}: "${args.input[key]}",`, "");
    return `input: {${data}}`;
};

const getQueryArguments = (args) => Object.keys(args).reduce((str, key) => str + `${key}: ${typeof args[key] === 'number'? args[key]: `"${args[key]}"`},`, "");
const loopQueryChildren = (children) => children.reduce((query, node) => query += ` ${stringBuilder(node)}`, "");


function stringBuilder(query) {
    let queryString = "";

    switch (query.type) {
        case "Query":
            queryString += `${query.name}(${ getQueryArguments(query.arguments) })`;
            break;
        case "Mutation":
            queryString += `${query.name}(${ getMutationInput(query.arguments) })`;
            break;
        case "Field":
        case "Connection":
        case "Record":
            queryString += `${query.name}`;
            break;
        default:
            queryString += ``;
            break;
    }
    if(query.children.length) queryString += `{${loopQueryChildren(query.children)}}`;
    return queryString;
}
