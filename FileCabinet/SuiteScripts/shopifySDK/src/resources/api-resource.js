/**
 * api-resource.js
 * @NApiVersion 2.1
 * @NModuleScope public
 *
 * Module is intended to act like an SDK for the Shopify Admin API
 * PATH: SuiteScripts/shopifySDK/api-resource
 *
 * @author Mitch Roberts
 * @version 2019.0.1, 25 Nov 2019
 */
define(['N/https'], function(_https){
    const build = ({url, auth} = {url: '', auth: ''}) => {
        return {
            send: function(data) {
                const response = _https.post({
                    url: url,
                    body: data,
                    headers: { "Content-Type": "application/graphql", "X-Shopify-Access-Token": auth }
                });

                if (response.code === 200) {
                    const body = JSON.parse(response.body);
                    if(body.hasOwnProperty('errors')) throw new Error(response.body);
                    else return body.data[Object.keys(body.data)[0]];
                }
                else throw new Error(response.body);
            }
        }
    };

    return {build};
});
