// Export Record.js

define([], function(){
    "use strict";
    class Record {
        constructor({fields = [], linkedFields = [], linkedRecords = [], mutationFields = [], mutations = [], queries = []}) {
            this.mutationFields = mutationFields;
            this.fields = fields;
            this.linkedFields = linkedFields;
            this.linkedRecords = linkedRecords;

            this.queries = queries;
            this.mutations = mutations;
        }
        query() {
            return this.queries.reduce(function(obj, cur) {
                const key = Object.keys(cur)[0];

                obj[key] = function ({parameters = {first: 1}, fields, linkedFields, linkedRecords}) {
                    if(!fields && !linkedFields && !linkedRecords) throw new Error('Field or Connection Required!');

                    let queryFields = fields && fields.hasOwnProperty('_get')? fields._get(): "";
                    let queryLinkedFields = linkedFields && linkedFields.hasOwnProperty('_get') ? linkedFields._get(): "";
                    let queryLinkedRecords= linkedRecords && linkedRecords.hasOwnProperty('_get')? linkedRecords._get(): "";

                    const queryParameters = Object.keys(parameters).reduce((str, key) => str + `${key}: ${parameters[key]},`, "");
                    return `${cur[key]}(${queryParameters}){ edges{ node {${queryFields} ${queryLinkedFields} ${queryLinkedRecords}} }}`;
                };

                return obj;
            }, {});
        }

        mutate() {
            return this.mutations.reduce(function (obj, cur) {
                const key = Object.keys(cur)[0];
                const endpoint = cur[key].endpoint;
                const record = cur[key].record;

                obj[key] = function({mutationInput, fields, linkedFields, linkedRecords}){
                    if(!mutationInput) throw new Error('Mutation Input Required!');

                    let queryFields = fields && fields.hasOwnProperty('_get')? fields._get(): "";
                    let queryLinkedFields = linkedFields && linkedFields.hasOwnProperty('_get') ? linkedFields._get(): "";
                    let queryLinkedRecords= linkedRecords && linkedRecords.hasOwnProperty('_get')? linkedRecords._get(): "";

                    return `${endpoint}(input: {${mutationInput._get()}}) {${record} { ${queryFields} ${queryLinkedFields} ${queryLinkedRecords} }}`;
                };


                return obj;
            }, {});
        }


        setMutationFields() {
            let mutationFields = [];
            return Object.assign(
                this.mutationFields.reduce(createMutationFieldsObject(mutationFields), {}),
                {_get: function(){ return mutationFields.join(' '); }}
            );
        }

        setQueryFields() {
            let fieldsArray = [];
            return Object.assign(
                this.fields.reduce(createFieldsObject(fieldsArray), {}),
                {_get: function(){ return fieldsArray.join(' '); }}
            );
        }
        setLinkedRecords() {
            let linkedRecordsArray = [];
            return Object.assign(
                this.linkedRecords.reduce(createLinkedRecordsObject(linkedRecordsArray)),
                {_get: function(){ return linkedRecordsArray.join(' '); }}
            );
        }
        setLinkedFields() {
            let linkedFieldsArray = [];
            return Object.assign(
                this.linkedFields.reduce(createLinkedFieldsObject(linkedFieldsArray)),
                {_get: function(){ return linkedFieldsArray.join(' '); }}
            );
        }
    }
    return Record;
});



function createMutationFieldsObject(mutationFields) {
    return function(obj, field) {
        obj[field] = function(updateValue){ mutationFields.push(`${field}: "${updateValue}",`); return this; }
        return obj;
    }
}



function createLinkedFieldsObject(linkedFieldsArray) {
    return function(obj, record) {
        obj[record] = function({fields, linkedFields, linkedRecords}) {
            let queryFields = fields? fields._get(): "";
            let queryLinkedFields = linkedFields? linkedFields._get(): "";
            let queryLinkedRecords = linkedRecords? linkedRecords._get(): "";

            linkedFieldsArray.push(`${record}{ ${queryFields} ${queryLinkedFields} ${queryLinkedRecords} }`);
            return this;
        };
        return obj;
    }
}
function createLinkedRecordsObject(linkedRecordsArray) {
    return function(obj, record) {
        obj[record] = function( recordQuery ){ linkedRecordsArray.push(recordQuery); return this; }
        return obj;
    }
}
function createFieldsObject(fieldsArray) {
    return function(obj, field) {
        obj[field] = function(){ fieldsArray.push(`${field}`); return this; }
        return obj;
    }
}

//function linkedGenerator(recordArray, callback, linkedArray) { return Object.assign( recordArray.reduce(callback(linkedArray)), {_get: function(){ return linkedArray.join(' '); }}); }
