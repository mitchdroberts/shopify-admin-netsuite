/**
 * variant-root.js
 * @NApiVersion 2.1
 * @NModuleScope public
 *
 * Module is intended to act like an SDK for the Shopify Admin API
 * PATH: SuiteScripts/shopifySDK/variant-resource
 *
 * @author Mitch Roberts
 * @version 2019.0.1, 25 Nov 2019
 */

define([], function() {
    const Variant = function() {};


    /* -- Queries -- */
    Variant.prototype.BuildQuery = function() {
        let queryObject;

        return {
            allVariants: function({first, after}){
                queryObject = {type: 'Query', name: 'productVariants', arguments: Object(...arguments), children: []};
                return {
                    edges: function() {
                        queryObject.children.push( {type: 'Connection', name: 'edges', arguments: {}, children: []} );
                        queryObject.children.push({type: 'Connection', name: 'pageInfo', arguments: {}, children: [
                                {type: 'Field', name: 'hasNextPage', arguments: {}, children: []},
                                {type: 'Field', name: 'hasPreviousPage', arguments: {}, children: []},
                            ]});
                        return {
                            node: function() {
                                const edgeObject = queryObject.children[0];
                                edgeObject.children.push( {type: 'Connection', name: 'node', arguments: {}, children: []} );
                                edgeObject.children.push({type: 'Field', name: 'cursor', arguments: {}, children: []});
                                return {
                                    append: function({nodes}) {
                                        edgeObject.children[0].children = nodes._get();
                                        return queryObject;
                                    }
                                }
                            }
                        }
                    }
                }
            },
            oneVariant: function({id}){
                queryObject = {type: 'Query', name: 'productVariant', arguments: Object(...arguments), children: []};
                return {
                    append: function({nodes}) {
                        queryObject.children = nodes._get();
                        return queryObject;
                    }
                }
            },
            searchVariants: function({first, after}, ...query){
                let parameters = Object(arguments[0]);
                parameters.query = query.reduce(function(string, value){
                    let key = Object.keys(value)[0];
                    return string += `${key}:'${value[key]}' `;
                },"");


                queryObject = {type: 'Query', name: 'productVariants', arguments: parameters, children: []};
                return {
                    edges: function() {
                        queryObject.children.push( {type: 'Connection', name: 'edges', arguments: {}, children: []} );
                        queryObject.children.push({type: 'Connection', name: 'pageInfo', arguments: {}, children: [
                                {type: 'Field', name: 'hasNextPage', arguments: {}, children: []},
                                {type: 'Field', name: 'hasPreviousPage', arguments: {}, children: []},
                            ]});
                        return {
                            node: function() {
                                const edgeObject = queryObject.children[0];
                                edgeObject.children.push( {type: 'Connection', name: 'node', arguments: {}, children: []} );
                                edgeObject.children.push({type: 'Field', name: 'cursor', arguments: {}, children: []});
                                return {
                                    append: function({nodes}) {
                                        edgeObject.children[0].children = nodes._get();
                                        return queryObject;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    Variant.prototype.queryNodes = function() {
        const queryNodes = [];
        return {
            id:
                function(){queryNodes.push( {type: 'Field', name: 'id', arguments: {}, children: []} ); return this;},
            sku:
                function(){queryNodes.push( {type: 'Field', name: 'sku', arguments: {}, children: []} ); return this;},
            barcode:
                function(){queryNodes.push( {type: 'Field', name: 'barcode', arguments: {}, children: []} ); return this;},
            product:
                function({nodes}) { queryNodes.push({type: 'Record', name: 'product', arguments: {}, children: nodes._get()}); return this; },
            metafields:
                function({query}){ queryNodes.push(query); return this; },
            _get:
                function(){ return queryNodes; }
        }
    };



    /* -- Mutations -- */
    //mutation {productVariantUpdate(input: {id: "hienhein", barcode: "stdhl87gd"}) {productVariant {barcode id}}}
    Variant.prototype.BuildMutation = function() {
        let mutationObject;
        return {
            updateVariant: function({input}) {
                mutationObject = {type: 'Mutation', name: 'productVariantUpdate', arguments: {input: input._get()}, children: []};
                return {//userErrors
                    product: function(){},
                    productVariant: function(){
                        mutationObject.children.push( {type: 'Record', name: 'productVariant', arguments: {}, children: []} );
                        return {
                            append: function({nodes}) {
                                mutationObject.children[0].children = nodes._get();
                                return mutationObject;
                            }
                        }
                    }
                }
            }
        }
    };


    Variant.prototype.mutationInput = function() {
        const mutationInputs = [];
        return {
            id:
                function({value}) { mutationInputs.push({type: 'Field', name: 'id', value: value}); return this;},
            barcode:
                function({value}) { mutationInputs.push({type: 'Field', name: 'barcode', value: value}); return this;},
            _get:
                function(){
                    return mutationInputs.reduce(
                        function(obj, item){ if(item.type === 'Field') obj[item.name] = item.value; return obj; },{});
                }
        }
    };


    return Variant;
});
