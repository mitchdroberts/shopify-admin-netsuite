/**
 * product-root.js
 * @NApiVersion 2.1
 * @NModuleScope public
 *
 * Module is intended to act like an SDK for the Shopify Admin API
 * PATH: SuiteScripts/shopifySDK/product-resource
 *
 * @author Mitch Roberts
 * @version 2019.0.1, 25 Nov 2019
 */
define([], function () {
    const QueryAll = queryHelper('productVariants');
    const QueryOne = queryHelper('productVariant');
    const QuerySearch = queryHelper('productVariants');

    function queryHelper(type) {
        return function ({parameters = {first: 1}, fields, linkedFields, linkedRecords}) {
            if(!fields && !linkedFields && !linkedRecords) throw new Error('Field or Connection Required!');

            let queryFields = fields && fields.hasOwnProperty('_get')? fields._get(): "";
            let queryLinkedFields = linkedFields && linkedFields.hasOwnProperty('_get') ? linkedFields._get(): "";
            let queryLinkedRecords= linkedRecords && linkedRecords.hasOwnProperty('_get')? linkedRecords._get(): "";

            const queryParameters = Object.keys(parameters).reduce((str, key) => str + `${key}: ${parameters[key]},`, "");
            return `${type}(${queryParameters}){ edges{ node {${queryFields} ${queryLinkedFields} ${queryLinkedRecords}} }}`;
        }
    }

    function setFields(){
        let fieldsArray = [];
        return {
            availablePublicationCount: function(){ fieldsArray.push('availablePublicationCount'); return this; },
            createdAt: function(){ fieldsArray.push('createdAt'); return this; },
            defaultCursor: function(){ fieldsArray.push('defaultCursor'); return this; },
            description: function(){ fieldsArray.push('description'); return this; },
            descriptionHtml: function(){ fieldsArray.push('descriptionHtml'); return this; },
            giftCardTemplateSuffix: function(){ fieldsArray.push('giftCardTemplateSuffix'); return this; },
            handle: function(){ fieldsArray.push('handle'); return this; },
            hasOnlyDefaultVariant: function(){ fieldsArray.push('hasOnlyDefaultVariant'); return this; },
            hasOutOfStockVariants: function(){ fieldsArray.push('hasOutOfStockVariants'); return this; },
            id: function(){ fieldsArray.push('id'); return this; },
            inCollection: function(){ fieldsArray.push('inCollection'); return this; },
            isGiftCard: function(){ fieldsArray.push('isGiftCard'); return this; },
            legacyResourceId: function(){ fieldsArray.push('legacyResourceId'); return this; },
            onlineStorePreviewUrl: function(){ fieldsArray.push('onlineStorePreviewUrl'); return this; },
            onlineStoreUrl: function(){ fieldsArray.push('onlineStoreUrl'); return this; },
            productType: function(){ fieldsArray.push('productType'); return this; },
            publicationCount: function(){ fieldsArray.push('publicationCount'); return this; },
            publishedAt: function(){ fieldsArray.push('publishedAt'); return this; },
            publishedOnCurrentPublication: function(){ fieldsArray.push('publishedOnCurrentPublication'); return this; },
            publishedOnPublication: function(){ fieldsArray.push('publishedOnPublication'); return this; },
            storefrontId: function(){ fieldsArray.push('storefrontId'); return this; },
            tags: function(){ fieldsArray.push('tags'); return this; },
            templateSuffix: function(){ fieldsArray.push('templateSuffix'); return this; },
            title: function(){ fieldsArray.push('title'); return this; },
            totalInventory: function(){ fieldsArray.push('totalInventory'); return this; },
            totalVariants: function(){ fieldsArray.push('totalVariants'); return this; },
            tracksInventory: function(){ fieldsArray.push('tracksInventory'); return this; },
            updatedAt: function(){ fieldsArray.push('updatedAt'); return this; },
            vendor: function(){ fieldsArray.push('vendor'); return this; },
            _get: function(){ return fieldsArray.join(' '); }
        }
    }
    function setLinkedFields() {
        let linkedFieldsArray = [];

        return {
            translations: function({fields, linkedFields, linkedRecords}){
                return linkedFieldsHelper(fields, linkedFields, linkedRecords, 'translations', linkedFieldsArray, this);
            },
            options: function({fields, linkedFields, linkedRecords}){
                return linkedFieldsHelper(fields, linkedFields, linkedRecords, 'options', linkedFieldsArray, this);
            },
            priceRange: function({fields, linkedFields, linkedRecords}){
                return linkedFieldsHelper(fields, linkedFields, linkedRecords, 'priceRange', linkedFieldsArray, this);
            },
            privateMetafield: function({fields, linkedFields, linkedRecords}){
                return linkedFieldsHelper(fields, linkedFields, linkedRecords, 'privateMetafield', linkedFieldsArray, this);
            },
            featuredImage: function({fields, linkedFields, linkedRecords}){
                return linkedFieldsHelper(fields, linkedFields, linkedRecords, 'featuredImage', linkedFieldsArray, this);
            },
            feedback: function({fields, linkedFields, linkedRecords}){
                return linkedFieldsHelper(fields, linkedFields, linkedRecords, 'feedback', linkedFieldsArray, this);
            },
            seo: function({fields, linkedFields, linkedRecords}){
                return linkedFieldsHelper(fields, linkedFields, linkedRecords, 'seo', linkedFieldsArray, this);
            },
            metafield: function({fields, linkedFields, linkedRecords}){
                return linkedFieldsHelper(fields, linkedFields, linkedRecords, 'metafield', linkedFieldsArray, this);
            },
            _get: function(){ return linkedFieldsArray.join(' '); }
        }
    }
    function setLinkedRecords() {
        let linkedRecordsArray = [];
        return {
            metafields: function( recordQuery ){ linkedRecordsArray.push(recordQuery); return this; },
            privateMetafields: function( recordQuery ){ linkedRecordsArray.push(recordQuery); return this; },
            collections: function( recordQuery ){ linkedRecordsArray.push(recordQuery); return this; },
            images: function( recordQuery ){ linkedRecordsArray.push(recordQuery); return this; },
            resourcePublications: function( recordQuery ){ linkedRecordsArray.push(recordQuery); return this; },
            unpublishedPublications: function( recordQuery ){ linkedRecordsArray.push(recordQuery); return this; },
            variants: function( recordQuery ){ linkedRecordsArray.push(recordQuery); return this; },
            _get: function(){ return linkedRecordsArray.join(' '); }
        }
    }

    function linkedFieldsHelper(fields, linkedFields, linkedRecords, mainFields, fieldArray, self) {
        let queryFields = fields? fields._get(): "";
        let queryLinkedFields = linkedFields? linkedFields._get(): "";
        let queryLinkedRecords = linkedRecords? linkedRecords._get(): "";

        fieldArray.push(`${mainFields}{ ${queryFields} ${queryLinkedFields} ${queryLinkedRecords} }`);
        return self;
    }


    return {QueryAll, QueryOne, QuerySearch, setFields, setLinkedFields, setLinkedRecords};
});
