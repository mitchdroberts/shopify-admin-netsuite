define([], function(){
    return {
        url: "https://<YOURSHOP>.myshopify.com/admin/api/<APIVERSION>/graphql.json",
        auth: "<APPAUTHORIZATIONTOKEN>"
    };
});